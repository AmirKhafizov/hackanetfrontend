import Vue from 'vue'
import App from './App.vue'
import router from './routes'
import VueRouter from 'vue-router'
import VueCookies from 'vue-cookies'

import Navigation from "./Navigation";
import Footer from "./Footer";

Vue.component('hn-nav', Navigation);
Vue.component('hn-footer', Footer);
Vue.use(VueRouter);
Vue.use(VueCookies);

new Vue({
  el: '#app',
  render: h => h(App),
  router: router
});
