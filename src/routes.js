import VueRouter from 'vue-router'
import Hackathons from "./Hackathons";
import SignIn from "./SignIn";
import Profile from "./Profile";
import EditProfile from "./EditProfile";
import Users from "./Users";
import AddHackathon from "./AddHackathon";
import SignUp from "./SignUp";
import Teams from "./Teams";
import Logout from "./Logout";
import AddTeam from "./AddTeam";
import TeamInvitations from "./TeamInvitations";
import UserProfile from "./UserProfile";
import ProfileEducationAndExperience from "./ProfileEducationAndExperience";
import HackathonApplication from "./HackathonApplication";
import Statistic from "./Statistic";
import Hackathon from "./Hackathon";
import HackathonStatistic from "./HackathonStatistic";
import Chats from "./Chats";
import Invitations from "./Invitations";
import Connections from "./Connections";
import Applications from "./Applications";
import JoinTeamRequests from "./JoinTeamRequests";
import Team from "./Team";

export default new VueRouter({
  routes: [
    {
      path: '/hackathons',
      component: Hackathons
    },
    {
      path: '/login',
      component: SignIn
    },
    {
      path: '/profile/home',
      component: Profile
    },
    {
      path: '/profile/info/:id',
      component: ProfileEducationAndExperience
    },
    {
      path: '/profiles/:id',
      component: UserProfile
    },
    {
      path: '/profile/edit',
      component: EditProfile
    },
    {
      path: '/users',
      component: Users
    },
    {
      path: '/hackathons/add',
      component: AddHackathon
    },
    {
      path: '/hackathons/apply/:id',
      component: HackathonApplication
    },
    {
      path: '/register',
      component: SignUp
    },
    {
      path: '/teams',
      component: Teams
    },
    {
      path: '/teams/add',
      component: AddTeam
    },
    {
      path: '/logout',
      component: Logout
    },
    {
      path: '/teams/invitations',
      component: TeamInvitations
    },
    {
      path: '/statistics',
      component: Statistic
    },
    {
      path: '/hackathons/:id',
      component: Hackathon
    },
    {
      path: '/hackathons/:id/statistic',
      component: HackathonStatistic
    },
    {
      path: '/chats',
      component: Chats
    },
    {
      path: '/invitations',
      component: Invitations
    },
    {
      path: '/connections',
      component: Connections
    },
    {
      path: '/hackathon/:id/applications',
      component: Applications
    },
    {
      path: '/requests',
      component: JoinTeamRequests
    },
    {
      path: '/team/:id',
      component: Team
    }
  ],
  mode: 'history'
})


